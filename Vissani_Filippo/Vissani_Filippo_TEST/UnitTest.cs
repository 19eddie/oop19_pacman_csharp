using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using Vissani_Filippo;

namespace Vissani_Filippo_TEST
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestBuilderNoFields()
        {
            Assert.ThrowsException<System.Exception>(() =>
            {
                new GameMap.Builder().MapSize(10, 10)
                .GhostsHouse(new HashSet<IPair<int, int>>())
                .Build();
            });
        }

        [TestMethod]
        public void TestGameMap()
        {
            ISet<IPair<int, int>> walls = new HashSet<IPair<int, int>>();
            ISet<IPair<int, int>> pills = new HashSet<IPair<int, int>>();
            ISet<IPair<int, int>> ghostsHouse = new HashSet<IPair<int, int>>();
            for (int i = 0; i < 40; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    walls.Add(new Pair<int, int>(i, j));
                }
            }
            ghostsHouse.Add(new Pair<int, int>(0, 20));
            ghostsHouse.Add(new Pair<int, int>(1, 20));
            for (int i = 0; i < 40; i++)
            {
                for (int j = 21; j < 40; j++)
                {
                    pills.Add(new Pair<int, int>(i, j));
                }
            }
            IGameMap gameMap = new GameMap.Builder()
                    .MapSize(40, 40)
                    .PacManStartPosition(new Pair<int, int>(2, 20))
                    .Walls(walls)
                    .GhostsHouse(ghostsHouse)
                    .Pills(pills)
                    .PillScore(10)
                    .Build();
            Assert.AreEqual<IPair<int, int>>(gameMap.GetPacManStartPosition(), new Pair<int, int>(2, 20));
            Assert.AreEqual<int>(gameMap.GetWallsPositions().Count(), 800);
            Assert.AreEqual<int>(gameMap.GetPillsPositions().Count(), 760);
        }

        [TestMethod]
        public void TestLevelManager()
        {
            ILevelManager levelManager = new LevelManager(60, 10, 10000);
            levelManager.DecLevelTime();
            levelManager.IncScores(100);
            Assert.AreEqual<int>(levelManager.GetScores(), 100);
            Assert.AreEqual<int>(levelManager.GetPartialScores(), 100);
            Assert.AreEqual<int>(levelManager.GetLevelTime(), 59);
            levelManager.NextLevel();
            levelManager.NextLevel();
            levelManager.NextLevel();
            Assert.AreEqual<int>(levelManager.GetPartialScores(), 0);
            Assert.IsTrue(!levelManager.IsGameInverted());
            Assert.AreEqual<int>(levelManager.GetLevelNumber(), 4);
        }
    }
}
