﻿using System;
using System.Collections.Generic;

namespace Vissani_Filippo
{
    /**
	 * This class is used to manage the map of the game, with walls pills, etc.
	 */
    public class GameMap : IGameMap
	{
		private static readonly int DEFAULT_PILL_SCORE = 100;
		private readonly int pillScore;
		private readonly Dictionary<TileType, ISet<IPair<int, int>>> gameMap;
		private readonly int xMapSize;
		private readonly int yMapSize;
		private readonly ISet<IPair<int, int>> initialPillsPostion;

		private GameMap(int xMapSize,
							int yMapSize,
							int pillPoints,
							ISet<IPair<int, int>> walls,
							ISet<IPair<int, int>> pills,
							ISet<IPair<int, int>> ghostsHouse,
							IPair<int, int> pacManStartPosition)
		{
			this.pillScore = pillPoints;
			this.xMapSize = xMapSize;
			this.yMapSize = yMapSize;
			this.initialPillsPostion = new HashSet<IPair<int, int>>(pills);
			this.gameMap = new Dictionary<TileType, ISet<IPair<int, int>>>();
			this.gameMap.Add(TileType.PACMAN_START, new HashSet<IPair<int, int>>());
			this.gameMap.GetValueOrDefault(TileType.PACMAN_START).Add(pacManStartPosition);
			this.gameMap.Add(TileType.WALL, walls);
			this.gameMap.Add(TileType.PILL, pills);
			this.gameMap.Add(TileType.GHOSTS_HOUSE, ghostsHouse);
			this.gameMap.Add(TileType.FREE, new HashSet<IPair<int, int>>());
		}

		public class Builder : IGameMapBuilder
		{
			private int xMapSize;
			private int yMapSize;
			private int pillScore;
			private ISet<IPair<int, int>> pills;
			private ISet<IPair<int, int>> walls;
			private ISet<IPair<int, int>> ghostsHouse;
			private ISet<IPair<int, int>> pacManStartPosition;

			public Builder MapSize(int xMapSize, int yMapSize)
			{
				this.xMapSize = xMapSize;
				this.yMapSize = yMapSize;
				return this;
			}

			public Builder PillScore(int pillScore)
			{
				this.pillScore = pillScore;
				return this;
			}

			public Builder Walls(ISet<IPair<int, int>> walls)
			{
				this.walls = walls;
				return this;
			}
		
			public Builder Pills(ISet<IPair<int, int>> pills)
			{
				this.pills = pills;
				return this;
			}
		
			public Builder GhostsHouse(ISet<IPair<int, int>> ghostsHouse)
			{
				this.ghostsHouse = ghostsHouse;
				return this;
			}
		
			public Builder PacManStartPosition(IPair<int, int> position)
			{
				this.pacManStartPosition = new HashSet<IPair<int, int>>();
				this.pacManStartPosition.Add(position);
				return this;
			}
		
			public GameMap Build()
			{
				if (this.ghostsHouse == null
					|| this.pills == null
					|| this.walls == null
					|| this.xMapSize <= 0
					|| this.yMapSize <= 0
					|| this.pacManStartPosition == null)
				{
					throw new Exception();
				}
				if (this.pillScore == 0)
				{
					this.pillScore = DEFAULT_PILL_SCORE;
				}
				IPair<int, int> tmp = null;
                foreach (var item in this.pacManStartPosition)
                {
					tmp = new Pair<int, int>(item.GetX(), item.GetY());
                }
				return new GameMap(this.xMapSize,
					this.yMapSize,
					this.pillScore,
					this.walls,
					this.pills,
					this.ghostsHouse,
					tmp);
			}
		}


		enum TileType
		{
			WALL,
			PILL,
			GHOSTS_HOUSE,
			PACMAN_START,
			FREE
		}

		public void RemovePill(IPair<int, int> position)
		{
            foreach (var item in this.gameMap.GetValueOrDefault(TileType.PILL))
            {
                if (position.GetX() == item.GetX() && position.GetY() == item.GetY())
                {
					this.gameMap.GetValueOrDefault(TileType.PILL).Remove(item);

				}
            }
			this.gameMap.GetValueOrDefault(TileType.FREE).Add(position);
		}

		public ISet<IPair<int, int>> GetWallsPositions()
		{
			return new HashSet<IPair<int, int>>(this.gameMap.GetValueOrDefault(TileType.WALL));
		}

		public ISet<IPair<int, int>> GetPillsPositions()
		{
			return new HashSet<IPair<int, int>>(this.gameMap.GetValueOrDefault(TileType.PILL));
		}

		public ISet<IPair<int, int>> GetGhostHousePosition()
		{
			return new HashSet<IPair<int, int>>(this.gameMap.GetValueOrDefault(TileType.GHOSTS_HOUSE));
		}

		public ISet<IPair<int, int>> GetNoWallsPositions()
		{
			ISet<IPair<int, int>> noWalls = new HashSet<IPair<int, int>>();
			noWalls.UnionWith(this.GetPillsPositions());
			noWalls.UnionWith(this.gameMap.GetValueOrDefault(TileType.FREE));
			noWalls.Add(this.GetPacManStartPosition());
			return noWalls;
		}

		public int GetxMapSize()
		{
			return this.xMapSize;
		}

		public int GetyMapSize()
		{
			return this.yMapSize;
		}

		public int GetPillScore()
		{
			return this.pillScore;
		}

		public bool IsPill(IPair<int, int> position)
		{
            foreach (var item in this.GetPillsPositions())
            {
                if (position.GetX() == item.GetX() && position.GetY() == item.GetY())
                {
					return true;
                }
            }
			return false;
		}

		public IPair<int, int> GetPacManStartPosition()
		{
            foreach (IPair<int, int> item in this.gameMap.GetValueOrDefault(TileType.PACMAN_START))
            {
				return item;
            }
			return null;
		}

		public void RestorePills()
		{
			this.gameMap.GetValueOrDefault(TileType.FREE).Clear();
			this.gameMap.GetValueOrDefault(TileType.PILL).UnionWith(this.initialPillsPostion);
		}
	}
}