﻿using System;
using System.Collections.Generic;

namespace Vissani_Filippo
{
	/**
	 * 
	 * this interface is used to manage the game map and all its immobile entities.
	 */
	public interface IGameMap
	{
		/**
		 * <param name="position">of the pill to remove</param>
		 */
		void RemovePill(IPair<int, int> position);
		/**
		 * <returns>the walls positions</returns>
		 */
		ISet<IPair<int, int>> GetWallsPositions();
		/**
		 * <returns>the pills positions</returns>
		 */
		ISet<IPair<int, int>> GetPillsPositions();
		/**
		 * <returns>the ghost house position</returns>
		 */
		ISet<IPair<int, int>> GetGhostHousePosition();
		/**
		 * <returns>free positions (no wall or ghost house)</returns>
		 */
		ISet<IPair<int, int>> GetNoWallsPositions();
		/**
		 * <returns>the map size on x axis</returns>
		 */
		int GetxMapSize();
		/**
		 * <returns>the map size on y axis</returns>
		 */
		int GetyMapSize();
		/**
		 * <returns>score value of one pill</returns>
		 */
		int GetPillScore();
		/**
		 * <param name="position">of the pill</param>
		 * <returns>true if in position there is a pill, false otherwise</returns>
		 */
		bool IsPill(IPair<int, int> position);
		/**
		 * <returns>the initial position of PacMan</returns>
		 */
		IPair<int, int> GetPacManStartPosition();
		/**
		 * Restores the pills in the map.
		 */
		void RestorePills();
	}
}
