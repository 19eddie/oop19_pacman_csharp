﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vissani_Filippo
{
	/*
	* This interface defines methods used to manage scores, levels and game state
	*/
	public interface ILevelManager
	{
		/**
		 * <returns>true if the game is inverted (PacMan can eat ghosts), false otherwise</returns>
		 */
		bool IsGameInverted();
		/**
		 * Decrease the level time
		 */
		void DecLevelTime();
		/**
		 * Increase the level
		 */
		void NextLevel();
		/**
		 * <param name = "value">increments scores and partial scores adding the input value</param>
		 */
		void IncScores(int value);
		/**
		* <returns>the duration (in seconds) of the level</returns>
		*/
		int GetLevelDuration();
		/**
		* <returns>the duration (in seconds) of the inverted game</returns>
		*/
		int GetInvertedGameDuration();
		/**
		* <returns>necessary scores to invert game</returns>
		*/
		int GetScoresToInvertGame();
		/**
		 * <returns>actual scores</returns>
		 */
		int GetScores();
		/**
		 * <returns>partial scores of the level</returns>
		 */
		int GetPartialScores();
		/**
		 * <returns>the level number</returns>
		 */
		int GetLevelNumber();
		/**
		 * <returns>the actual level time (in seconds)</returns>
		 */
		int GetLevelTime();
		/**
		 * <returns>the actual inverted game time (in seconds), if the game is not inverted returns 0</returns>
		 */
		int GetInvertedGameTime();
	}
}
