﻿using System;

namespace Vissani_Filippo
{
	/**
	*
	* <param name = "X"></param>
	* <param name = "Y"></param>
	* 
	* this interface manages a pair of objects
	*/
	public interface IPair<X, Y>
	{
		/**
		 * <returns>the element of type X</returns>
		 */
		X GetX();
		/**
		 * <returns>the element of type Y</returns>
		 */
		Y GetY();
	}
}
