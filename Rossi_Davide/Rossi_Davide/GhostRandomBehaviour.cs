﻿using System.Collections.Generic;
using static Rossi_Davide.Directions;
using System;
using System.Linq;

namespace Rossi_Davide
{
    public class GhostRandomBehaviour : GhostAbstractBehaviour
    {
        public GhostRandomBehaviour(int xMapSize, int yMapSize, IPair<int, int> startPosition,
                List<IPair<int, int>> ghostHouse, HashSet<IPair<int, int>> walls )
            : base(xMapSize, yMapSize, startPosition, ghostHouse, walls)
        {

        }

        public void Move(bool timeToTurn)
        {
            if (timeToTurn)
            {
                TurnAround();
            }
            else
            {
                RunAway();
            }
        }

        private void TurnAround()
        {
            SetAdj(CurrentPosition);
            if (CurrentDirection.Equals(UP))
            {
                CurrentPosition = GetAdj(DOWN);
                CurrentDirection = DOWN;
            }
            else if (CurrentDirection.Equals(RIGHT))
            {
                CurrentPosition = GetAdj(LEFT);
                CurrentDirection = LEFT;
            }
            else if (CurrentDirection.Equals(DOWN))
            {
                CurrentPosition = GetAdj(UP);
                CurrentDirection = UP;
            }
            else
            {
                CurrentPosition = GetAdj(RIGHT);
                CurrentDirection = RIGHT;
            }
        }

        private void RunAway()
        {
            Random r = new Random();
            IPair<int, int> oldPosition = CurrentPosition;
            SetAdj(CurrentPosition);
            Dictionary<Directions, IPair<int, int>> map = new Dictionary<Directions, IPair<int, int>>();
            Dictionary<Directions, IPair<int, int>> map2;
            map.Add(UP, GetAdj(UP));
            map.Add(RIGHT, GetAdj(RIGHT));
            map.Add(DOWN, GetAdj(DOWN));
            map.Add(LEFT, GetAdj(LEFT));
            while (CurrentPosition.Equals(oldPosition))
            {
                foreach (KeyValuePair<Directions, IPair<int, int>> entry in map)
                {
                    if (CurrentDirection.Equals(entry.Key))
                    {
                        map2 = new Dictionary<Directions, IPair<int, int>>(map);
                        map2.Remove(OppositeDirection(entry.Key));
                        List<Directions> list = new List<Directions>(map2.Keys);
                        Directions randomDir = list[r.Next(3)];
                        map2.TryGetValue(randomDir, out IPair<int, int> value);
                        if (!Walls.Any(pos => pos.GetX().Equals(value.GetX()) && pos.GetY().Equals(value.GetY())))
                        {
                            CurrentPosition = value;
                            CurrentDirection = randomDir;
                            return;
                        }
                    }
                }
            }
        }

        public override void CheckIfInside()
        {
            base.CheckIfInside();
        }

        public override void ReturnHome(IPair<int, int> newPosition)
        {
            base.ReturnHome(newPosition);
        }
    }
}

