﻿using System.Collections.Generic;
using System.Linq;
using static Rossi_Davide.Directions;

namespace Rossi_Davide
{
    public abstract class GhostSmartAbstractBehaviour : GhostAbstractBehaviour
    { 

        private const int UPPERBOUND = 10_000;

        private readonly Dictionary<IPair<int, int>, int> mapDijkstra;
        private readonly HashSet<IPair<int, int>> walls;
        private readonly int xMapSize;
        private readonly int yMapSize;
        private bool isPathFound;
        private bool isBlinkyDead;
        private bool relaxed;
        private readonly IPair<int, int> outsideTarget;

        public GhostSmartAbstractBehaviour(HashSet<IPair<int, int>> walls, IPacman pacMan,
                List<IPair<int, int>> ghostHouse, int xMapSize, int yMapSize,
                IPair<int, int> startPosition) 
            : base(xMapSize, yMapSize, startPosition, ghostHouse, walls)
        {
            
            mapDijkstra = new Dictionary<IPair<int, int>, int>();
            isPathFound = false;
            isBlinkyDead = false;
            relaxed = true;
            this.xMapSize = xMapSize;
            this.yMapSize = yMapSize;
            this.walls = Walls;
            Pacman = pacMan;
            outsideTarget = new Pair<int, int>(startPosition.GetX(), startPosition.GetY() - 3);
        }

        protected void Relax(Ghosts name, bool eatable)
        {
            if (IsInside() && (name.Equals(Ghosts.RANDY) || eatable))
            {
                RelaxTarget = outsideTarget;
            }
            FindPath(RelaxTarget);
            if (CurrentPosition.Equals(RelaxTarget))
            {
                relaxed = false;
            }
        }
        protected void FindPath(IPair<int, int> targetPosition)
        {
            Dictionary<IPair< int, int >, int> queue = new Dictionary<IPair<int, int>, int>();
            int distance = 0;
            int value;
            isPathFound = false;
            for (int x = 0; x < xMapSize; x++)
            {
                for (int y = 0; y < yMapSize; y++)
                {
                    mapDijkstra.Add(new Pair<int, int>(x, y), UPPERBOUND);
                }
            }
            mapDijkstra.Add(CurrentPosition, distance++);
            SetAdj(CurrentPosition);
            if (GetAdj(UP).GetY() >= 0 && !walls.Any(pos => pos.GetX().Equals(GetAdj(UP).GetX()) && pos.GetY().Equals(GetAdj(UP).GetY())) && !CurrentDirection.Equals(DOWN))
            {
                mapDijkstra.Add(GetAdj(UP), distance);
                queue.Add(GetAdj(UP), distance);
            }
            if (GetAdj(DOWN).GetY() < yMapSize && !walls.Any(pos => pos.GetX().Equals(GetAdj(DOWN).GetX()) && pos.GetY().Equals(GetAdj(DOWN).GetY())) && !CurrentDirection.Equals(UP))
            {
                mapDijkstra.Add(GetAdj(DOWN), distance);
                queue.Add(GetAdj(DOWN), distance);
            }
            if (GetAdj(RIGHT).GetX() < xMapSize && !walls.Any(pos => pos.GetX().Equals(GetAdj(RIGHT).GetX()) && pos.GetY().Equals(GetAdj(RIGHT).GetY())) && !CurrentDirection.Equals(LEFT))
            {
                mapDijkstra.Add(GetAdj(RIGHT), distance);
                queue.Add(GetAdj(RIGHT), distance);
            }
            if (GetAdj(LEFT).GetX() >= 0 && !walls.Any(pos => pos.GetX().Equals(GetAdj(LEFT).GetX()) && pos.GetY().Equals(GetAdj(LEFT).GetY())) && !CurrentDirection.Equals(RIGHT))
            {
                mapDijkstra.Add(GetAdj(LEFT), distance);
                queue.Add(GetAdj(LEFT), distance);
            }
            mapDijkstra.TryGetValue(targetPosition, out value);
            if (value < UPPERBOUND)
            {
                isPathFound = true;
            }
            while (!isPathFound)
            {
                Dictionary<IPair< int, int>, int> queueCopy = new Dictionary<IPair<int, int>, int>(queue);
                foreach (KeyValuePair<IPair< int, int>, int> entry in queueCopy )
                {
                    SetAdj(entry.Key);
                    mapDijkstra.TryGetValue(GetAdj(UP), out value);
                    if (GetAdj(UP).GetY() >= 0 && !walls.Any(pos => pos.GetX().Equals(GetAdj(UP).GetX()) && pos.GetY().Equals(GetAdj(UP).GetY())) && distance < value)
                    {
                        mapDijkstra.Add(GetAdj(UP), distance + 1);
                        queue.Add(GetAdj(UP), distance + 1);
                    }
                    mapDijkstra.TryGetValue(GetAdj(RIGHT), out value);
                    if (GetAdj(RIGHT).GetX() < xMapSize && !walls.Any(pos => pos.GetX().Equals(GetAdj(RIGHT).GetX()) && pos.GetY().Equals(GetAdj(RIGHT).GetY())) && distance < value)
                    {
                        mapDijkstra.Add(GetAdj(RIGHT), distance + 1);
                        queue.Add(GetAdj(RIGHT), distance + 1);
                    }
                    mapDijkstra.TryGetValue(GetAdj(LEFT), out value);
                    if (GetAdj(LEFT).GetX() >= 0 && !walls.Any(pos => pos.GetX().Equals(GetAdj(LEFT).GetX()) && pos.GetY().Equals(GetAdj(LEFT).GetY())) && distance < value)
                    {
                        mapDijkstra.Add(GetAdj(LEFT), distance + 1);
                        queue.Add(GetAdj(LEFT), distance + 1);
                    }
                    mapDijkstra.TryGetValue(GetAdj(DOWN), out value);
                    if (GetAdj(DOWN).GetY() < yMapSize && !walls.Any(pos => pos.GetX().Equals(GetAdj(DOWN).GetX()) && pos.GetY().Equals(GetAdj(DOWN).GetY())) && distance < value)
                    {
                        mapDijkstra.Add(GetAdj(DOWN), distance + 1);
                        queue.Add(GetAdj(DOWN), distance + 1);
                    }
                    mapDijkstra.TryGetValue(targetPosition, out value);
                    if (value < UPPERBOUND)
                    {
                        isPathFound = true;
                    }
                    queue.Remove(entry.Key);
                }
                distance++;
            }
            Move(targetPosition);
        }
        protected void Move(IPair<int, int> targetPosition)
        {
            IPair<int, int> lastPosition = CurrentPosition;
            int i;
            CurrentPosition = targetPosition;
            mapDijkstra.TryGetValue(CurrentPosition, out i);
            mapDijkstra.TryGetValue(GetAdj(UP), out var up);
            mapDijkstra.TryGetValue(GetAdj(DOWN), out var down);
            mapDijkstra.TryGetValue(GetAdj(LEFT), out var left);
            mapDijkstra.TryGetValue(GetAdj(RIGHT), out var right);
            while (i > 1)
            {
                SetAdj(CurrentPosition);
                if (GetAdj(UP).GetY() >= 0 && !walls.Any(pos => pos.GetX().Equals(GetAdj(UP).GetX()) && pos.GetY().Equals(GetAdj(UP).GetY())) && up.Equals(i - 1))
                {
                    CurrentPosition = GetAdj(UP);
                }
                else if (GetAdj(RIGHT).GetX() < xMapSize && !walls.Any(pos => pos.GetX().Equals(GetAdj(RIGHT).GetX()) && pos.GetY().Equals(GetAdj(RIGHT).GetY())) && right.Equals(i - 1))
                {
                    CurrentPosition = GetAdj(RIGHT);
                }
                else if (GetAdj(DOWN).GetY() < yMapSize && !walls.Any(pos => pos.GetX().Equals(GetAdj(DOWN).GetX()) && pos.GetY().Equals(GetAdj(DOWN).GetY())) && down.Equals(i - 1))
                {
                    CurrentPosition = GetAdj(DOWN);
                }
                else if (GetAdj(LEFT).GetX() >= 0 && !walls.Any(pos => pos.GetX().Equals(GetAdj(LEFT).GetX()) && pos.GetY().Equals(GetAdj(LEFT).GetY())) && left.Equals(i - 1))
                {
                    CurrentPosition = GetAdj(LEFT);
                }
                mapDijkstra.TryGetValue(CurrentPosition, out i);
            }
            SetAdj(CurrentPosition);
            if (lastPosition.Equals(GetAdj(UP)))
            {
                CurrentDirection = DOWN;
            }
            else if (lastPosition.Equals(GetAdj(RIGHT)))
            {
                CurrentDirection = LEFT;
            }
            else if (lastPosition.Equals(GetAdj(DOWN)))
            {
                CurrentDirection = UP;
            }
            else
            {
                CurrentDirection = RIGHT;
            }
        }
        protected bool MoveIfStuck()
        {
            SetAdj(CurrentPosition);
            Dictionary<Directions, IPair<int, int>> mapAdj = new Dictionary<Directions, IPair<int, int>>
            {
                { UP, GetAdj(UP) },
                { RIGHT, GetAdj(RIGHT) },
                { DOWN, GetAdj(DOWN) },
                { LEFT, GetAdj(LEFT) }
            };
            mapAdj.Remove(OppositeDirection(CurrentDirection));
            Dictionary<Directions, IPair< int, int >> mapAdjCopy = new Dictionary<Directions, IPair<int, int>>(mapAdj);
            foreach (KeyValuePair<Directions, IPair<int, int>> entry in mapAdj)
            {
                if (walls.Any(pos => pos.GetX().Equals(GetAdj(entry.Key).GetX()) && pos.GetY().Equals(GetAdj(entry.Key).GetY())))
                {
                    mapAdjCopy.Remove(entry.Key);
                }
            }
            if (mapAdjCopy.Count == 1)
            {
                foreach (KeyValuePair<Directions, IPair<int, int>> entry in mapAdjCopy)
                {
                    CurrentPosition = GetAdj(entry.Key);
                    CurrentDirection = entry.Key;
                }
                if (CurrentPosition.Equals(RelaxTarget))
                {
                    relaxed = false;
                }
                return true;
            }
            return false;
        }

        public IPair<int, int> RelaxTarget { get; set; }

        protected bool IsBlinkyDead()
        {
            return isBlinkyDead;
        }

        public void SetBlinkyDead()
        {
            isBlinkyDead = true;
        }

        public bool IsRelaxed()
        {
            return relaxed;
        }

        protected void SetRelaxed(bool relaxed)
        {
            this.relaxed = relaxed;
        }

        protected IPacman Pacman { get; }

    }

}

