﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rossi_Davide
{
    /*
     * BARZI_EDDIE
     */
    public class PacmanImpl : EntityAbstractImpl, IPacman
    {
        public Directions CurrentDirection { get; set; }
        public int Lives { get; private set; }
        private IPair<int, int> position;
        private readonly IPair<int, int> startPosition;
        private readonly ISet<IPair<int, int>> noWalls;

        /**
        * Private constructor. is called by the builder.
        */
        private PacmanImpl(int xMapSize, int yMapSize, IPair<int, int> startPosition,
                int lives, ISet<IPair<int, int>> noWalls, Directions currentDirection) : base(xMapSize, yMapSize)
        {
            this.noWalls = noWalls;
            this.startPosition = startPosition;
            this.position = startPosition;
            this.CurrentDirection = currentDirection;
            this.Lives = lives;
        }

        /**
         * inner builder class to build the PacManImpl object.
         */
        public class Builder
        {
            private int? xMapSize = null;
            private int? yMapSize = null;
            private IPair<int, int> startPosition = null;
            private int? lives = null;
            private ISet<IPair<int, int>> noWalls = null;
            private Directions? currentDirection = null;
            private bool built;

            /**
             * Used by the builder to check if the Optional fields are correctly assigned.
             */
            private static void Check(bool b)
            {
                if (!b)
                {
                    throw new InvalidOperationException();
                }
            }

            /**
             * <param name="x">xMapSize</param>
             * <param name="y">yMapSize</param>
             * <returns>this</returns>
             */
            public Builder MapSize(int x, int y)
            {
                this.xMapSize = x;
                this.yMapSize = y;
                return this;
            }
            /**
             * <param name="startPosition">a pair containing the x,y position </param>
             * <returns>this</returns>
             */
            public Builder StartPosition(IPair<int, int> startPosition)
            {
                this.startPosition = startPosition;
                return this;
            }

            /**
             * <param name="lives">number of lives.</param>
             * <returns>this</returns>
             */
            public Builder Lives(int lives)
            {
                this.lives = lives;
                return this;
            }

            /**
             * <param name="noWalls">a set containing the coordinates where you can go</param>
             * <returns>this</returns>
             */
            public Builder NoWalls(ISet<IPair<int, int>> noWalls)
            {
                this.noWalls = noWalls;
                return this;
            }

            /**
             * <param name="currentDirection">the current direction. can be empty if pacman is stall</param>
             * <returns>this</returns>
             */
            public Builder CurrentDirection(Directions currentDirection)
            {
                this.currentDirection = currentDirection;
                return this;
            }


            /**
             * 
             * <returns>a new instance of PacManImpl</returns>
             * <exception cref="InvalidOperationException">if some parameter is missed</exception>
             */
            public PacmanImpl Build()
            {
                Check(!this.built);
                Check(this.xMapSize != null);
                Check(this.yMapSize != null);
                Check(this.lives != null);
                Check(this.startPosition != null);
                Check(this.currentDirection != null);
                Check(this.noWalls != null);
                this.built = true;

                return new PacmanImpl(this.xMapSize ?? default, this.yMapSize ?? default, this.startPosition,
                        this.lives ?? default, this.noWalls, this.currentDirection ?? default);
            }
        }
        /**
         * Move the PacMan to the next position.
         */
        public void NextPosition()
        {
            IPair<int, int> next = this.ConvertToToroidal(this.CalculateNextPosition());
            if (this.noWalls.Any(pos => pos.GetX().Equals(next.GetX()) && pos.GetY().Equals(next.GetY())))
            {
                this.SetPosition(next);
            }
        }
        /**
         * calculate the next position of pacman basing on actual position and actual direction.
         */
        private Pair<int, int> CalculateNextPosition()
        {
            int x = 0;
            int y = 0;
            switch (this.CurrentDirection)
            {
                case Directions.UP:
                    y = -1;
                    break;
                case Directions.DOWN:
                    y = 1;
                    break;
                case Directions.LEFT:
                    x = -1;
                    break;
                case Directions.RIGHT:
                    x = 1;
                    break;
                default:
                    break;
            }
            return new Pair<int, int>(this.position.GetX() + x, this.position.GetY() + y);
        }

        /**
         * <inheritdoc/>
         */
        public void Kill()
        {
            this.Lives -= 1;
            this.ReturnToStartPosition();
        }

        /**
         * <inheritdoc/>
         */
        public IPair<int, int> GetPosition()
        {
            return this.position;
        }

        /**
         * <inheritdoc/>
         */
        public void ReturnToStartPosition()
        {
            this.SetPosition(startPosition);
        }

        /**
         * Set the position of PacMan.
         * <param name="position">the position of PacMan</param>
         */
        private void SetPosition(IPair<int, int> position)
        {
            this.position = position;
        }
    }
}

