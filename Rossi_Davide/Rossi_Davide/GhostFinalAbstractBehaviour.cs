﻿using System.Collections.Generic;

namespace Rossi_Davide
{
    public abstract class GhostFinalAbstractBehaviour : GhostSmartAbstractBehaviour
    {

        private readonly GhostRandomBehaviour rBehaviour;

        public GhostFinalAbstractBehaviour(HashSet<IPair<int, int>> walls, IPacman pacMan,
                List<IPair<int, int>> ghostHouse, int xMapSize, int yMapSize,
                IPair<int, int> startPosition) 
            : base(walls, pacMan, ghostHouse, xMapSize, yMapSize, startPosition)
            {
                rBehaviour = new GhostRandomBehaviour(xMapSize, yMapSize,startPosition, ghostHouse, walls);
                CurrentPosition = startPosition;
            }
        public void NextPosition(bool eatable, bool timeToTurn, Ghosts name)
            {
                CheckIfInside();
                if (!timeToTurn && MoveIfStuck())
                {
                    rBehaviour.CurrentDirection = CurrentDirection;
                    rBehaviour.CurrentPosition = CurrentPosition;
                }
                else
                {
                    if (timeToTurn || (eatable || name.Equals(Ghosts.RANDY)) && !IsRelaxed())
                    {
                        rBehaviour.Move(timeToTurn);
                        CurrentDirection = rBehaviour.CurrentDirection;
                        CurrentPosition = rBehaviour.CurrentPosition;
                    }
                    else
                    {
                        if (IsRelaxed())
                        {
                            Relax(name, eatable);
                        }
                        else
                        {
                            FindPath(GetChaseTarget());
                        }
                        rBehaviour.CurrentDirection = CurrentDirection;
                        rBehaviour.CurrentPosition = CurrentPosition;
                    }
                }
            }

        public override void ReturnHome(IPair<int, int> newPosition)
            {
                base.ReturnHome(newPosition);
                rBehaviour.ReturnHome(newPosition);
                SetRelaxed(true);
            }
        public override void CheckIfInside()
            {
                base.CheckIfInside();
                rBehaviour.CheckIfInside();
            }

        protected abstract IPair<int, int> GetChaseTarget();

    }

}

