﻿using System.Collections.Generic;

namespace Rossi_Davide
{
    public class GhostBlinkyBehaviour : GhostFinalAbstractBehaviour
    {
        public GhostBlinkyBehaviour(HashSet<IPair<int, int>> setWall, IPacman pacMan,
            List<IPair<int, int>> ghostHouse, int xMapSize, int yMapSize,
            IPair<int, int> relaxTarget, IPair<int, int> startPosition)
            : base(setWall, pacMan, ghostHouse, xMapSize, yMapSize, startPosition)
        {
            RelaxTarget = relaxTarget;
        }
        protected override IPair<int, int> GetChaseTarget()
        {
            return Pacman.GetPosition();
        }

    }
}
