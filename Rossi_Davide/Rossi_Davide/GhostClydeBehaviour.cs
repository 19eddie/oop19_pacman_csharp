﻿using System.Collections.Generic;
using System.Linq;

namespace Rossi_Davide
{
    public class GhostClydeBehaviour : GhostFinalAbstractBehaviour
    {
        private IPair<int, int> chaseTarget;
        private static readonly int PACMANRADIUS = 7;

        public GhostClydeBehaviour(HashSet<IPair<int, int>> setWall, IPacman pacMan,
                List<IPair<int, int>> ghostHouse, int xMapSize, int yMapSize,
                IPair<int, int> relaxTarget, IPair<int, int> startPosition)
            : base(setWall, pacMan, ghostHouse, xMapSize, yMapSize, startPosition)
        {

            RelaxTarget = relaxTarget;
        }

        protected override IPair<int, int> GetChaseTarget()
        {
            HashSet<IPair<int, int>> walls = Walls;
            if (CurrentPosition.GetX() > Pacman.GetPosition().GetX() - PACMANRADIUS && CurrentPosition.GetX() < Pacman.GetPosition().GetX() + PACMANRADIUS
                && CurrentPosition.GetY() > Pacman.GetPosition().GetY() - PACMANRADIUS && CurrentPosition.GetY() < Pacman.GetPosition().GetY() + PACMANRADIUS)
            {
                chaseTarget = RelaxTarget;
                if (CurrentPosition.Equals(chaseTarget))
                {
                    if (chaseTarget.GetY() + 1 < YMapSize && !walls.Any(pos => pos.GetX().Equals(chaseTarget.GetX()) && pos.GetY().Equals(chaseTarget.GetY() + 1)))
                    {
                        chaseTarget = new Pair<int, int>(chaseTarget.GetX(), chaseTarget.GetY() + 1);
                    }
                    else if (chaseTarget.GetX() + 1 < XMapSize && !walls.Any(pos => pos.GetX().Equals(chaseTarget.GetX() + 1) && pos.GetY().Equals(chaseTarget.GetY())))
                    {
                        chaseTarget = new Pair<int, int>(chaseTarget.GetX() + 1, chaseTarget.GetY());
                    }
                    else if (chaseTarget.GetX() - 1 >= 0 && !walls.Any(pos => pos.GetX().Equals(chaseTarget.GetX() - 1) && pos.GetY().Equals(chaseTarget.GetY())))
                    {
                        chaseTarget = new Pair<int, int>(chaseTarget.GetX() - 1, chaseTarget.GetY());
                    }
                    else if (chaseTarget.GetY() - 1 >= 0 && !walls.Any(pos => pos.GetX().Equals(chaseTarget.GetX()) && pos.GetY().Equals(chaseTarget.GetY() - 1)))
                    {
                        chaseTarget = new Pair<int, int>(chaseTarget.GetX(), chaseTarget.GetY() - 1);
                    }
                }
                return chaseTarget;
            }
            else
            {
                return Pacman.GetPosition();
            }
        }
    }
}
