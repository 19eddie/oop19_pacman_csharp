﻿namespace Rossi_Davide
{
    public enum Ghosts
    {
        BLINKY,
        PINKY,
        INKY,
        CLYDE,
        RANDY
    }
}
