﻿using System.Collections.Generic;
using System.Linq;
using static Rossi_Davide.Directions;

namespace Rossi_Davide
{
    public abstract class GhostAbstractBehaviour
    {
        private Dictionary<Directions, IPair<int, int>> mapAdj;
        private bool inside;
        private readonly List<IPair<int, int>> ghostHouse;

        public GhostAbstractBehaviour(int xMapSize, int yMapSize, IPair<int, int> startPosition,
                List<IPair<int, int>> ghostHouse, HashSet<IPair<int, int>> walls)
        {
            XMapSize = xMapSize;
            YMapSize = yMapSize;
            CurrentDirection = UP;
            CurrentPosition = startPosition;
            inside = true;
            this.ghostHouse = ghostHouse;
            Walls = new HashSet<IPair<int, int>>(walls);
        }

        protected int XMapSize { get; }

        protected int YMapSize { get; }

        public Directions CurrentDirection { get; set; }

        public IPair<int, int> CurrentPosition { get; set; }

        protected IPair<int, int> GetAdj(Directions dir)
        {
            mapAdj.TryGetValue(dir, out IPair<int, int> value);
            return value;
        }

        protected void SetAdj(IPair<int, int> position)
        {
            mapAdj = new Dictionary<Directions, IPair<int, int>>
            {
                {UP, new Pair<int, int>(position.GetX(), position.GetY() - 1) },
                { DOWN, new Pair<int, int>(position.GetX(), position.GetY() + 1) },
                { RIGHT, new Pair<int, int>(position.GetX() + 1, position.GetY()) },
                { LEFT, new Pair<int, int>(position.GetX() - 1, position.GetY()) }
            };
        }

        public virtual void CheckIfInside()
        {
            if (inside && !ghostHouse.Any(pos => pos.GetX().Equals(CurrentPosition.GetX()) && pos.GetY().Equals(CurrentPosition.GetY())))
            {
                Walls.UnionWith(ghostHouse);
                inside = false;
            }
        }

        public virtual void ReturnHome(IPair<int, int> newPosition)
        {
            inside = true;
            CurrentPosition = newPosition;
            CurrentDirection = UP;
            foreach (var x in ghostHouse)
            {
                Walls.Remove(x);
            }
        }

        protected HashSet<IPair<int, int>> Walls { get; }

        public bool IsInside()
        {
            return inside;
        }

        protected Directions OppositeDirection(Directions dir)
        {
            if (dir.Equals(UP))
            {
                return DOWN;
            }
            else if (dir.Equals(LEFT))
            {
                return RIGHT;
            }
            else if (dir.Equals(DOWN))
            {
                return UP;
            }
            else
            {
                return LEFT;
            }
        }

    }
}
