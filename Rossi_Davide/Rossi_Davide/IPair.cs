﻿namespace Rossi_Davide
{
	/**
	* FILIPPO VISSANI
	*/
	public interface IPair<X, Y>
	{
		/**
		* <returns>the element of type X</returns>
		*/
		X GetX();
		/**
		* <returns>the element of type Y</returns>
		*/
		Y GetY();
	}
}
