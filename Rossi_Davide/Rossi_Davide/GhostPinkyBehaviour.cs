﻿using System.Collections.Generic;
using System.Linq;

namespace Rossi_Davide
{
    public class GhostPinkyBehaviour : GhostFinalAbstractBehaviour
    {

        private IPair<int, int> chaseTarget;
        public GhostPinkyBehaviour(HashSet<IPair<int, int>> setWall, IPacman pacMan,
                List<IPair<int, int>> ghostHouse, int xMapSize, int yMapSize,
                IPair<int, int> relaxTarget, IPair<int, int> startPosition)
            : base(setWall, pacMan, ghostHouse, xMapSize, yMapSize, startPosition)
        {
            RelaxTarget = relaxTarget;
        }

        protected override IPair<int, int> GetChaseTarget() {
            HashSet<IPair< int, int>> walls = Walls;
            IPair<int, int> pacManPosition = Pacman.GetPosition();
            Directions pacManDirection = Pacman.CurrentDirection;
            for (int i = 0; i <= 4; i++)
            {
                if (pacManDirection.Equals(Directions.UP))
                {
                    if (!walls.Any(pos => pos.GetX().Equals(pacManPosition.GetX()) && pos.GetY().Equals(pacManPosition.GetY() - i)) && pacManPosition.GetY() - i >= 0)
                    {
                        chaseTarget = new Pair<int, int>(pacManPosition.GetX(), pacManPosition.GetY() - i);
                    }
                }
                else if (pacManDirection.Equals(Directions.RIGHT))
                {
                    if (!walls.Any(pos => pos.GetX().Equals(pacManPosition.GetX() + i) && pos.GetY().Equals(pacManPosition.GetY())) && pacManPosition.GetX() + i < XMapSize)
                    {
                        chaseTarget = new Pair<int, int>(pacManPosition.GetX() + i, pacManPosition.GetY());
                    }
                }
                else if (pacManDirection.Equals(Directions.DOWN))
                {
                    if (!walls.Any(pos => pos.GetX().Equals(pacManPosition.GetX()) && pos.GetY().Equals(pacManPosition.GetY() + i)) && pacManPosition.GetY() + i < YMapSize)
                    {
                        chaseTarget = new Pair<int, int>(pacManPosition.GetX(), pacManPosition.GetY() + i);
                    }
                }
                else
                {
                    if (!walls.Any(pos => pos.GetX().Equals(pacManPosition.GetX() - i) && pos.GetY().Equals(pacManPosition.GetY())) && pacManPosition.GetX() - i >= 0)
                    {
                        chaseTarget = new Pair<int, int>(pacManPosition.GetX() - i, pacManPosition.GetY());
                    }
                }
            }

            if (CurrentPosition.Equals(chaseTarget))
            {
                if (chaseTarget.GetY() + 1 < YMapSize && !walls.Any(pos => pos.GetX().Equals(chaseTarget.GetX()) && pos.GetY().Equals(chaseTarget.GetY() + 1)))
                {
                    chaseTarget = new Pair<int, int>(chaseTarget.GetX(), chaseTarget.GetY() + 1);
                }
                else if (chaseTarget.GetX() + 1 < XMapSize && !walls.Any(pos => pos.GetX().Equals(chaseTarget.GetX() + 1) && pos.GetY().Equals(chaseTarget.GetY())))
                {
                    chaseTarget = new Pair<int, int>(chaseTarget.GetX() + 1, chaseTarget.GetY());
                }
                else if (chaseTarget.GetX() - 1 >= 0 && !walls.Any(pos => pos.GetX().Equals(chaseTarget.GetX() - 1) && pos.GetY().Equals(chaseTarget.GetY())))
                {
                    chaseTarget = new Pair<int, int>(chaseTarget.GetX() - 1, chaseTarget.GetY());
                }
                else if (chaseTarget.GetY() - 1 >= 0 && !walls.Any(pos => pos.GetX().Equals(chaseTarget.GetX()) && pos.GetY().Equals(chaseTarget.GetY() - 1)))
                {
                    chaseTarget = new Pair<int, int>(chaseTarget.GetX(), chaseTarget.GetY() - 1);
                }
            }
            return chaseTarget;
        }
    }
}
