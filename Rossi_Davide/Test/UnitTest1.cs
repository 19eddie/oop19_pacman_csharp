using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rossi_Davide;
using System.Collections.Generic;

namespace Rossi_Davide_TEST
{
    [TestClass]
    public class UnitTest1
    {
        private GhostFinalAbstractBehaviour blinky;
        private GhostFinalAbstractBehaviour pinky;
        private GhostFinalAbstractBehaviour clyde;
        private IPacman pacMan;
        private readonly int xMapSize = 10;
        private readonly int yMapSize = 10;
        private readonly IPair<int, int> startPosition = new Pair<int, int>(5, 4);
        private HashSet<IPair<int, int>> walls = new HashSet<IPair<int, int>>();
        private HashSet<IPair<int, int>> noWalls = new HashSet<IPair<int, int>>();
        private List<IPair<int, int>> ghostHouse = new List<IPair<int, int>>();

        [TestInitialize]
        public void TestInitialize()
        {
            ghostHouse.AddRange(new List<Pair<int, int>>
            {
                 new Pair<int, int>(5, 4),
                 new Pair<int, int>(5, 5),
            });
            walls.UnionWith(new List<Pair<int, int>>
            {
                 new Pair<int, int>(4, 3),
                 new Pair<int, int>(4, 4),
                 new Pair<int, int>(4, 5),
                 new Pair<int, int>(5, 3),
                 new Pair<int, int>(6, 3),
                 new Pair<int, int>(6, 4),
                 new Pair<int, int>(6, 5)
            });
            pacMan = new PacmanImpl.Builder()
                        .CurrentDirection(Directions.LEFT)
                        .Lives(3)
                        .MapSize(xMapSize, yMapSize)
                        .NoWalls(noWalls)
                        .StartPosition(new Pair<int, int>(0, 0))
                        .Build();
            blinky = new GhostBlinkyBehaviour(walls, pacMan, ghostHouse, xMapSize, yMapSize, new Pair<int, int>(xMapSize - 2, 1), startPosition);
            pinky = new GhostPinkyBehaviour(walls, pacMan, ghostHouse, xMapSize, yMapSize, new Pair<int, int>(xMapSize - 2, 1), startPosition);
            clyde = new GhostClydeBehaviour(walls, pacMan, ghostHouse, xMapSize, yMapSize, new Pair<int, int>(xMapSize - 2, 1), startPosition);
        }

        [TestMethod()]
        public void StartPositionTest()
        {
            Assert.AreEqual(blinky.CurrentPosition, startPosition);
            Assert.AreEqual(pinky.CurrentPosition, startPosition);
            Assert.AreEqual(clyde.CurrentPosition, startPosition);
        }

        [TestMethod()]
        public void CheckIfInsideTest()
        {
            Assert.IsTrue(blinky.IsInside());
            Assert.IsTrue(pinky.IsInside());
            Assert.IsTrue(clyde.IsInside());
            blinky.NextPosition(false, false, Ghosts.BLINKY);
            pinky.NextPosition(false, false, Ghosts.PINKY);
            clyde.NextPosition(false, false, Ghosts.CLYDE);
            Assert.IsTrue(blinky.IsInside());
            Assert.IsTrue(pinky.IsInside());
            Assert.IsTrue(clyde.IsInside());
            blinky.NextPosition(false, false, Ghosts.BLINKY);
            pinky.NextPosition(false, false, Ghosts.PINKY);
            clyde.NextPosition(false, false, Ghosts.CLYDE);
            Assert.IsFalse(blinky.IsInside());
            Assert.IsFalse(pinky.IsInside());
            Assert.IsFalse(clyde.IsInside());
        }

        [TestMethod()]
        public void RelaxTargetReachedTest()
        {
            while (blinky.IsRelaxed())
            {
                blinky.NextPosition(false, false, Ghosts.BLINKY);
            }
            Assert.AreEqual(blinky.CurrentPosition, blinky.RelaxTarget);

            while (pinky.IsRelaxed())
            {
                pinky.NextPosition(false, false, Ghosts.PINKY);
            }
            Assert.AreEqual(pinky.CurrentPosition, pinky.RelaxTarget);

            while (clyde.IsRelaxed())
            {
                clyde.NextPosition(false, false, Ghosts.CLYDE);
            }
            Assert.AreEqual(clyde.CurrentPosition, clyde.RelaxTarget);

        }

        [TestMethod()]
        public void ReturnHomeTest()
        {
            while (blinky.IsInside())
            {
                blinky.NextPosition(false, false, Ghosts.BLINKY);
            }

            while (pinky.IsInside())
            {
                pinky.NextPosition(false, false, Ghosts.PINKY);
            }

            while (clyde.IsInside())
            {
                clyde.NextPosition(false, false, Ghosts.CLYDE);
            }
            blinky.ReturnHome(startPosition);
            pinky.ReturnHome(startPosition);
            clyde.ReturnHome(startPosition);
            Assert.AreEqual(blinky.CurrentPosition, startPosition);
            Assert.AreEqual(pinky.CurrentPosition, startPosition);
            Assert.AreEqual(clyde.CurrentPosition, startPosition);
        }
    }
}
