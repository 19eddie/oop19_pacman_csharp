﻿using System;
using System.Collections.Generic;

namespace PacMan
{
    /**
     * This interface represents the main class of the model.
     * Its purpose is to manage all the logic of the game, such as the position of the entities, the scores, etc.
     * Once the object has been created, set the game map by calling setGameMap(GameMap gameMap) and then initializeNewGame()
     */
    public interface IGameModel
    {
        /**
         * <param name="direction"> direction of Pac-Man</param>
         * sets the direction of Pac-Man
         */
        void SetPacManDirection(Directions direction);
        /**
         * <returns> a Set containing the wall's positions</returns>
         */
        ISet<IPair<int, int>> GetWallsPositions();
        /**
         * <returns>a Set containing the pill's positions</returns>
         */
        ISet<IPair<int, int>> GetPillsPositions();
        /**
         *<returns>the level duration</returns>
         */
        int GetLevelDuration();
        /**
         * <return>a Map containing KEY -> Id, VALUE -> position.</return> 
         */
        Dictionary<int, IPair<int, int>> GetGhostsPositions();
        /**
         * <returns> a Map containing KEY -> Id, VALUE -> type</returns>
         */
        Dictionary<int, Ghosts> GetGhostsTypes();
        /**
         * <returns>a Map containing KEY -> Id, VALUE -> direction.</returns>
         */
        Dictionary<int, Directions> GetGhostsDirections();
        /**
         *
         * <returns>true if the game is inverted, false otherwise</returns>
         */
        Boolean IsGameInverted();
        /**
         * Moves each mobile entity to its next position.
         */
        void MoveEntitiesNextPosition();
        /**
         * Reset the game.
         */
        void InitializeNewGame();

        /**
         * Increments the level time.
         */
        void DecLevelTime();
        /**
         * <returns> the level number</returns>
         */
        int GetLevelNumber();
        /**
         * <returns> the level time</returns>
         */
        int GetLevelTime();
        /**
         * <returns>the scores of the current game</returns> 
         */
        int GetScores();
        /**
         * <returns> the remaining lives of Pac-Man</returns>
         */
        int GetPacManLives();
        /**
         * <returns>the Pacman position</returns> 
         */
        IPair<int, int> GetPacManPosition();
        /**
         * <returns> the Pacman direction.</returns>
         */
        Directions GetPacManDirection();
        /**
         * <returns>true if the game is ended, false otherwise</returns> 
         */
        Boolean IsGameEnded();
        /**
         * <returns>the X GameMap size</returns> 
         */
        int GetyMapSize();
        /**
         * <returns> the Y GameMap size</returns>
         */
        int GetxMapSize();
        /**
         * Sets the game map.
         *
         * <param name="score"> gameMap sets the game map</param>
         */
        void SetGameMap(GameMap gameMap);
        /**
         *<returns> the time of inverted game </returns>
         */
        int GetInvertedGameTime();
    }
}
