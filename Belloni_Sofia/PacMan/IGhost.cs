﻿using System;

namespace PacMan
{
    interface IGhost : IEntity
    {
        /**
         * Creates the Ghost.
         */
        void Create();

        /**
         * Checks if the ghost is eatable.
         *
         * <returns> true, if is eatable</returns>
         */
        Boolean IsEatable();

        /**
         * Sets the eatable.
         *
         * <param name="eatable"> the new eatable</param>
         */
        void SetEatable(Boolean eatable);

        /**
         * Gets the name.
         *
         * <returns> the name </returns>
         */
        Ghosts GetName();

        /**
         *Notify Blinky's death.
         *
         */
        void BlinkyIsDead();

        /**
         * Gets the id.
         *
         * <returns> the ghost id</returns>
         */
        int GetId();

        /**
         * Gets the direction.
         *
         * <returns> the ghost direction</returns>
         */
        Directions GetDirection();

        /**
         * Sets the ghost name.
         *
         * <param name="name">the new name</param>
         */
        void SetName(Ghosts name);

    }
}
