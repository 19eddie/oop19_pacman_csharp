﻿namespace PacMan
{
    /**
     * BARZI EDDIE
    * This interface represents a generic game entity (ghosts, pacman, etc.).
    */
    public interface IEntity
    {
        /**
        * Calculates Entity next position.
        */
        void NextPosition();
        /**
         * Gets the position.
         * <returns>the position of the entity</returns>
         */
        IPair<int, int> GetPosition();
        /**
         * Return to the startPosition.
         */
        void ReturnToStartPosition();
    }
}
