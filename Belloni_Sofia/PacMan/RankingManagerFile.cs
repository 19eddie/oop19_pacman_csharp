﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PacMan
{
    /**
     * Class for saving the players' ranking on file and for its recovery from file.
     */
    public class RankingManagerFile : IRankingManager
    {      
        private const string DEFAULT_DIR = ".PacMan";
        private const string DEFAULT_FILE = "PacManScore.json";
        private const int MAX_SAVED_PLAYERS = 20;

        private readonly string home = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        private readonly char separator = Path.DirectorySeparatorChar;
        private readonly string pathFile;

        private List<Player> playersList = new List<Player>();

        /**
         * Constructor.
         */
        public RankingManagerFile()
        {
            this.pathFile = home + separator + DEFAULT_DIR + separator + DEFAULT_FILE;
            if (!Directory.Exists(home + separator + DEFAULT_DIR))
            {
                Directory.CreateDirectory(home + separator + DEFAULT_DIR);
            }
            if (!File.Exists(pathFile))
            {
                var fileStream = File.Create(pathFile);
                fileStream.Close();
            }
            else
            {
                this.Read();
            }
        }

        public int HighScore()
        {
            if (this.playersList.Count == 0)
            {
                return 0;
            } else
            {
                return this.playersList[0].Score;
            }  
        }

        public List<Player> GetAllPlayers()
        {
            return this.playersList;
        }

        public void Reset()
        {
            if (this.playersList.Count() != 0)
            {
                this.playersList.Clear();
                File.Delete(pathFile);
                var fileStream = File.Create(pathFile);
                fileStream.Close();
            }
        }

        public void SavePlayer(string name, int level, int score)
        {
            Player player = new Player(name, level, score);
            playersList.Add(player);
            this.playersList.Sort((a, b) => b.CompareByScore(a));
            if (this.playersList.Count > MAX_SAVED_PLAYERS)
            {
                this.playersList.RemoveAt(this.playersList.Count - 1);
            }
            this.Write();
        }

        private void Write()
        {
            string jsonString = JsonConvert.SerializeObject(this.playersList, Formatting.Indented);
            File.WriteAllText(pathFile, jsonString);
        }

        private void Read()
        {
            string jsonString = File.ReadAllText(pathFile);
            if (!String.IsNullOrEmpty(jsonString))
            {
                playersList = JsonConvert.DeserializeObject<List<Player>>(jsonString);
                this.playersList.Sort((a, b) => b.CompareByScore(a));
            }

        }
    }
}
