﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace PacMan
{
    public class GameModel : IGameModel
    {
        private const int PAC_MAN_LIVES = 3;
        private const int LEVEL_DURATION = 60;
        private const int INVERTED_GAME_DURATION = 10;

        private ISet<IGhost> ghosts;
        private IPacman pacMan;
        private IGameMap? gameMap = null;
        private LevelManager levelManager;
        private Collisions collisions;


        [MethodImpl(MethodImplOptions.Synchronized)]
        public void InitializeNewGame()
        {
            this.gameMap.RestorePills();
            this.levelManager = new LevelManager(LEVEL_DURATION,
                INVERTED_GAME_DURATION,
                gameMap.GetPillsPositions().Count() * gameMap.GetPillScore() / 4);
            this.ghosts = new HashSet<IGhost>();
            this.pacMan = new PacmanImpl.Builder()
                .CurrentDirection(Directions.UP)
                .MapSize(this.gameMap.GetxMapSize(), this.gameMap.GetyMapSize())
                .Lives(PAC_MAN_LIVES)
                .NoWalls(this.gameMap.GetNoWallsPositions())
                .StartPosition(this.gameMap.GetPacManStartPosition())
                .Build();
            this.CreateGhost(Ghosts.CLYDE);
            this.CreateGhost(Ghosts.INKY);
            this.CreateGhost(Ghosts.PINKY);
            this.collisions = new Collisions();
            this.collisions.SetPacManPosition(this.GetPacManPosition());
            //this.collisions.SetGhostsPositions(this.GetGhostsPositions());
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void DecLevelTime()
        {
            this.CheckGameEnded();
            if(this.levelManager.GetLevelTime()==0)
            {
                this.NextLevel();
            }
            else
            {
                bool oldIsGameInverted = this.levelManager.IsGameInverted();
                this.levelManager.DecLevelTime();
                if (oldIsGameInverted && !this.levelManager.IsGameInverted())
                { 
                    foreach(var ghost in ghosts)
                    {
                        ghost.SetEatable(false);
                    }
                }
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Dictionary<int, Directions> GetGhostsDirections()
        {
            var ghostsDirections = new Dictionary<int, Directions>();
            foreach (var ghost in ghosts)
            {
                ghostsDirections.Add(ghost.GetId(), ghost.GetDirection());
            }
            return ghostsDirections;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Dictionary<int, Ghosts> GetGhostsTypes()
        {
            var ghostTypes = new Dictionary<int, Ghosts>();
            foreach (var ghost in ghosts)
            {
                ghostTypes.Add(ghost.GetId(), ghost.GetName());
            }
            return ghostTypes;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Dictionary<int, IPair<int, int>> GetGhostsPositions()
        {
            var ghostPositions = new Dictionary<int, IPair<int, int>>();
            foreach (var ghost in ghosts)
            {
                ghostPositions.Add(ghost.GetId(), ghost.GetPosition());
            }
            return ghostPositions;

        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool IsGameInverted()
        {
            return this.levelManager.IsGameInverted();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int GetInvertedGameTime()
        {
            return this.levelManager.GetInvertedGameTime();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int GetLevelDuration()
        {
            return this.levelManager.GetLevelDuration();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int GetLevelNumber()
        {
            return this.levelManager.GetLevelNumber();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int GetLevelTime()
        {
            return this.levelManager.GetLevelTime();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool IsGameEnded()
        {
            return this.pacMan.Lives == 0;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SetGameMap(GameMap gameMap)
        {
            this.gameMap = gameMap;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SetPacManDirection(Directions direction)
        {
            this.CheckGameEnded();
            this.pacMan.CurrentDirection = direction;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Directions GetPacManDirection()
        {
            return this.pacMan.CurrentDirection;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int GetPacManLives()
        {
            return this.pacMan.Lives;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IPair<int, int> GetPacManPosition()
        {
            return this.pacMan.GetPosition();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ISet<IPair<int, int>> GetPillsPositions()
        {
            ISet<IPair<int, int>> copy = new HashSet<IPair<int, int>>();
            foreach (var x in this.gameMap.GetPillsPositions())
            {
                copy.Add(x);
            }
            return copy;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int GetScores()
        {
            return this.levelManager.GetScores();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ISet<IPair<int, int>> GetWallsPositions()
        {
            ISet<IPair<int, int>> copy = new HashSet<IPair<int, int>>();
            foreach (var x in this.gameMap.GetWallsPositions())
            {
                copy.Add(x);
            }
            return copy;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int GetxMapSize()
        {
            return this.gameMap.GetxMapSize();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int GetyMapSize()
        {
            return this.gameMap.GetyMapSize();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void MoveEntitiesNextPosition()
        {
            this.CheckGameEnded();
            //this.collisions.SetGhostsPositions(this.GetGhostsPositions());
            this.collisions.SetPacManPosition(this.GetPacManPosition());
            if (this.collisions.CheckPacManPillCollision(this.GetPillsPositions()))
            {
                this.gameMap.RemovePill(this.pacMan.GetPosition());
                bool oldIsGameInverted = this.levelManager.IsGameInverted();
                this.levelManager.IncScores(this.gameMap.GetPillScore());
                if (!oldIsGameInverted && this.levelManager.IsGameInverted())
                {
                    foreach (var ghost in this.ghosts)
                    {
                        ghost.SetEatable(true);
                    }
                }
                ISet<int> ghostsCollisions = this.collisions.CheckPacManGhostsCollision();
                if (ghostsCollisions.Count()!=0)
                {
                    if (this.levelManager.IsGameInverted())
                    {
                        foreach (var x in ghostsCollisions)
                        {
                            foreach (var y in this.ghosts)
                            {
                                if (x.Equals(y.GetId()))
                                {
                                    this.ghosts.Remove(y);
                                }
                            }
                        }
                    } else
                    {
                        this.pacMan.Kill();
                        foreach (var ghost in this.ghosts)
                        {
                            ghost.ReturnToStartPosition();
                        }
                    }
                }
                this.pacMan.NextPosition();
                foreach (var ghost in this.ghosts)
                {
                    //ghost.NextPosition();
                }
            }
        }

        private void CheckGameEnded()
        {
            this.CheckCondition(this.IsGameEnded() && this.gameMap != null);
        }

        private void CheckCondition(Boolean condition)
        {
            if (condition)
            {
                throw new InvalidOperationException();
            }
        }

        private void NextLevel()
        {
            this.levelManager.NextLevel();
            foreach (var ghost in ghosts)
            {
                ghost.ReturnToStartPosition();
                ghost.SetEatable(false);
                ghost.SetName(Ghosts.RANDY);
            }
            this.gameMap.RestorePills();
            this.pacMan.ReturnToStartPosition();
            this.CreateGhost(Ghosts.CLYDE);
            this.CreateGhost(Ghosts.INKY);
            this.CreateGhost(Ghosts.PINKY);
        }

        private void CreateGhost(Ghosts ghostName)
        {
            IGhost ghost;
            if (ghostName.Equals(Ghosts.BLINKY))
            {
                /* ghostFactory has not been implemented in c# */
                //ghost = this.ghostFactory.Blinky();
            }
            else if (ghostName.Equals(Ghosts.PINKY))
            {
                //ghost = this.ghostFactory.Pinky();
            }
            else if (ghostName.Equals(Ghosts.INKY))
            {
                //Ghost blinky = this.ghostFactory.Blinky();
                //blinky.Create();
                //ghost = this.ghostFactory.inky(blinky);
                this.ghosts.Add(new Ghost());
            }
            else
            {
                //ghost = this.ghostFactory.clyde();
            }
            //ghost.Create();
            this.ghosts.Add(new Ghost());
        }
    }

}
