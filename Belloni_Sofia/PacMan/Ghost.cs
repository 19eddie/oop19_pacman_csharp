﻿namespace PacMan
{
    //this class has been created only to be able to run GameModel, all methods are not implemented.
    internal class Ghost : IGhost
    {
        public void BlinkyIsDead()
        {
        }

        public void Create()
        {
        }

        public Directions GetDirection()
        {
            throw new System.NotImplementedException();
        }

        public int GetId()
        {
            throw new System.NotImplementedException();
        }

        public Ghosts GetName()
        {
            throw new System.NotImplementedException();
        }

        public IPair<int, int> GetPosition()
        {
            throw new System.NotImplementedException();
        }

        public bool IsEatable()
        {
            throw new System.NotImplementedException();
        }

        public void NextPosition()
        {
        }

        public void ReturnToStartPosition()
        {
        }

        public void SetEatable(bool eatable)
        {
        }

        public void SetName(Ghosts name)
        {
        }
    }
}