﻿using System.Collections.Generic;

namespace PacMan
{
    /**
     * Interface for managing the ranking.
     */
    public interface IRankingManager
    {
        /**
        * <return>list with all the Players saved.</return>
        */
        List<Player> GetAllPlayers();
        /**
         * <return>the current high-score.</return>
         */
        int HighScore();
        /**
         * Saves a new player. 
         * <param name="name""> the name of the player.</param>
         * <param name="level"> the level reached by the player.</param>
         * <param name="score">the score reached by the player.</param>
         */
        void SavePlayer(string name, int level, int score);
        /**
         * Deletes all the players saved. 
         */
        void Reset();
    }
}
