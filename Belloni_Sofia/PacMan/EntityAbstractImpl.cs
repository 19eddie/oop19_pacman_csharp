﻿namespace PacMan
{
    /**
     * BARZI EDDIE
     * this class implements a generic Entity that can be moved.
     */
    public abstract class EntityAbstractImpl
    {
        private int XMapSize { get; }
        private int YMapSize { get; }

        /**
         * Initializes default data for this entity.
         */
        public EntityAbstractImpl(int XMapSize, int YMapSize)
        {
            this.XMapSize = XMapSize;
            this.YMapSize = YMapSize;
        }

        /**
         * Method to move the entity in a toroidal world. if the entity want to move to a point outside the map this 
         * method return a position at the opposite of the map (where there is the tunnel)
         * <param name="position">position where the entity want to move</param> 
         * <returns>the correct position of the entity</returns> 
         */
        protected Pair<int, int> ConvertToToroidal(Pair<int, int> position)
        {
            int newX = position.GetX();
            int newY = position.GetY();
            if (newX >= this.XMapSize)
            {
                newX = 0;
            }
            if (newY >= this.YMapSize)
            {
                newY = 0;
            }
            if (newX < 0)
            {
                newX = this.XMapSize - 1;
            }
            if (newY < 0)
            {
                newY = this.YMapSize - 1;
            }
            return new Pair<int, int>(newX, newY);
        }
    }
}
