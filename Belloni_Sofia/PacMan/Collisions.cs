﻿using System.Collections.Generic;

namespace PacMan
{
    /*
     * 
     * This class is used to manage the collisions in the game
     *
     */
    public class Collisions
    {
        private Dictionary<int, IPair<int, int>>? ghostsPositions = null;
        private Dictionary<int, IPair<int, int>>? oldGhostsPositions = null;
        private IPair<int, int>? pacManPosition = null;
        private IPair<int, int>? oldPacManPosition = null;
        /**
         *
         * <param name="ghostsPositions"></param>
         * Sets the actual ghosts positions
         */
        public void SetGhostsPositions(Dictionary<int, IPair<int, int>> ghostsPositions)
        {
            if (this.ghostsPositions!=null)
            {
                this.oldGhostsPositions = this.ghostsPositions;
            }
            this.ghostsPositions = ghostsPositions;
        }
        /**
         *
         * <param name="='pacManPosition'"></param>
         * Sets the actual Pac Man position
         */
        public void SetPacManPosition(IPair<int, int> pacManPosition)
        {
            if (this.pacManPosition!=null)
            {
                this.oldPacManPosition = this.pacManPosition;
            }
            this.pacManPosition = pacManPosition;
        }
        /**
         *
         * <returns> Actual ghosts positions</returns>
         */
        public Dictionary<int, IPair<int, int>> GetGhostsPositions() {
            return this.ghostsPositions;
        }
        /**
         *
         * <returns> Old ghosts positions</returns>
         */
        public Dictionary<int, IPair<int, int>> GetOldGhostsPositions() {
            return this.oldGhostsPositions;
        }
        /**
         *
         * <returns> Actual Pac Man position</returns>
         */
        public IPair<int, int> GetPacManPosition() {
            return this.pacManPosition;
        }
        /**
         *
         * <returns>old Pac Man position</returns>
         */
        public IPair<int, int> GetOldPacManPosition() {
            return this.oldPacManPosition;
        }
        /**
         *
         * <param name="pillsPositions"></param>
         * <returns>true il pillPositions contains Pac Man position, false otherwise</returns>
         */
        public bool CheckPacManPillCollision(ISet<IPair<int, int>> pillsPositions)
        {
            return pillsPositions.Contains(this.pacManPosition);
        }
        /**
         *
         * <returns>A set with the ID of the ghosts that collide with Pac Man</returns>
         */
        public ISet<int> CheckPacManGhostsCollision() {
            ISet<int> ghosts = new HashSet<int>();
            foreach (var x in this.ghostsPositions.Keys)
            {
                if (this.ghostsPositions[x].Equals(this.pacManPosition))
                {
                    ghosts.Add(x);
                }
                if (this.oldGhostsPositions != null)
                {
                    foreach (var y in this.oldGhostsPositions.Keys)
                    {
                        if (x.Equals(y) && this.ghostsPositions[x].Equals(this.oldGhostsPositions)
                            && this.oldGhostsPositions[y].Equals(this.pacManPosition))
                        {
                            ghosts.Add(x);
                        }
                    }
                }
            }
            return ghosts;
        }
    }
}
