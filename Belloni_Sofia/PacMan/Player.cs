﻿namespace PacMan
{
    public class Player
    {
        public string Name { get; }
        public int Level { get; }
        public int Score { get; }

        /**
         * Constructor.
         * <param name="name""> the name of the player </param>
         * <param name="level">the level reached by the player</param>
         * <param name="score">the score reached by the player</param>
         */
        public Player(string name, int level, int score)
        {
            this.Name = name;
            this.Level = level;
            this.Score = score;
        }

        /**
         * This method compares the score of two players.
         * <param name="anotherPlayer">the Player to compare.</param>         *      
         * <returns>a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.</returns> 
         *      
         */
        public int CompareByScore(Player anotherPlayer)
        {
            return Score.CompareTo(anotherPlayer.Score);
        }

        /**
         * This method compares the level of two players.
         * <param name="anotherPlayer">the Player to compare.</param>         *      
         * <returns>a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.</returns> 
         *  
         */
        public int CompareByLevel(Player anotherPlayer)
        {
            return Level.CompareTo(anotherPlayer.Level);
        }

        public override string ToString()
        {
            return "Player [name=" + Name + ", level=" + Level + ", score=" + Score + "]";
        }
    }
}