﻿using System.Collections.Generic;

namespace PacMan
{
	/*
	* VISSANI FILIPPO
	* This interface defines the methods implemented by the game map Builder
	*
	*/
	public interface IGameMapBuilder
	{
		/**
		 * <param name="xMapSize"></param>
		 * <param name="yMapSize"></param>
		 * <returns>this</returns>
		 */
		GameMap.Builder MapSize(int xMapSize, int yMapSize);
		/**
		 * <param name="pillScore">score value of one pill</param>
		 * <returns>this</returns>
		 */
		GameMap.Builder PillScore(int pillScore);
		/**
		 * <param name="walls">a set containing all the positions of the walls</param>
		 * <returns>this</returns>
		 */
		GameMap.Builder Walls(ISet<IPair<int, int>> walls);
		/**
		 * <param name="pills">a set containing all the positions of the pills</param>
		 * <returns>this</returns>
		 */
		GameMap.Builder Pills(ISet<IPair<int, int>> pills);
		/**
		 * <param name="ghostsHouse">a set containing all the positions of the ghost house</param>
		 * <returns>this</returns>
		 */
		GameMap.Builder GhostsHouse(ISet<IPair<int, int>> ghostsHouse);
		/**
		 * <param name="position">initial position of PacMan</param>
		 * <returns>this</returns>
		 */
		GameMap.Builder PacManStartPosition(IPair<int, int> position);
		/**
		 * <returns>a GameMap object if all fields are not empty</returns>
		 */
		GameMap Build();
	}
}