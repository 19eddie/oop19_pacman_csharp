﻿namespace PacMan
{
	/*
	 * VISSANI FILIPPO
	 */
    public class Pair<X, Y> : IPair<X, Y>
	{
		readonly private X x;
		readonly private Y y;

		public Pair(X x, Y y)
		{
			this.x = x;
			this.y = y;
		}

		public X GetX()
		{
			return this.x;
		}

		public Y GetY()
		{
			return this.y;
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (obj == null)
			{
				return false;
			}
			if (GetType() != obj.GetType())
			{
				return false;
			}
			Pair<X, Y> other = (Pair<X, Y>)obj;
			if (x == null)
			{
				if (other.x != null)
				{
					return false;
				}
			}
			else if (!x.Equals(other.x))
			{
				return false;
			}
			if (y == null)
			{
				if (other.y != null)
				{
					return false;
				}
			}
			else if (!y.Equals(other.y))
			{
				return false;
			}
			return true;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public override string ToString()
		{
			return "Pair [x=" + x + ", y=" + y + "]";
		}
	}
}
