﻿using System;

namespace PacMan
{
	/*
	 * VISSANI FILIPPO
	 */
    public class LevelManager : ILevelManager
	{
		private readonly int levelDuration;
		private readonly int invertedGameDuration;
		private readonly int scoresToInvertGame;
		private int scores;
		private int partialScores;
		private int levelNumber;
		private int levelTime;
		private int invertedGameTime;

		public LevelManager(int levelDuration,
			int invertedGameDuration,
			int scoresToInvertGame)
		{
			this.levelDuration = levelDuration;
			this.invertedGameDuration = invertedGameDuration;
			this.scoresToInvertGame = scoresToInvertGame;
			this.scores = 0;
			this.levelNumber = 1;
			this.levelTime = levelDuration;
			this.invertedGameTime = 0;
		}

		public void DecLevelTime()
		{
			if (this.levelTime > 0)
			{
				this.levelTime = this.levelTime - 1;
			}
			else
			{
				throw new InvalidOperationException();
			}
			if (this.invertedGameTime > 0)
			{
				this.invertedGameTime = this.invertedGameTime - 1;
			}
		}

		public int GetInvertedGameDuration()
		{
			return this.invertedGameDuration;
		}

		public int GetInvertedGameTime()
		{
			return this.invertedGameTime;
		}

		public int GetLevelDuration()
		{
			return this.levelDuration;
		}

		public int GetLevelNumber()
		{
			return this.levelNumber;
		}

		public int GetLevelTime()
		{
			return this.levelTime;
		}

		public int GetPartialScores()
		{
			return this.partialScores;
		}

		public int GetScores()
		{
			return this.scores;
		}

		public int GetScoresToInvertGame()
		{
			return this.scoresToInvertGame;
		}

		public void IncScores(int value)
		{
			this.partialScores = this.partialScores + value;
			this.scores = this.scores + value;
			if (this.partialScores >= this.scoresToInvertGame)
			{
				this.SetInvertedGame();
				this.partialScores = 0;
			}
		}

		public bool IsGameInverted()
		{
			return this.invertedGameTime > 0;
		}

		public void NextLevel()
		{
			this.partialScores = 0;
			this.levelNumber = this.levelNumber + 1;
			this.levelTime = this.levelDuration;
			this.invertedGameTime = 0;
		}

		private void SetInvertedGame()
		{
			this.invertedGameTime = this.invertedGameDuration;
		}
	}
}
