using Microsoft.VisualStudio.TestTools.UnitTesting;
using PacMan;
using System.Collections.Generic;
using System.Linq;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestRankingManager()
        {
            IRankingManager manager = new RankingManagerFile();
            List<Player> scoreList;
            manager.Reset();
            scoreList = manager.GetAllPlayers();
            Assert.AreEqual(0, scoreList.Count);
            //test saving new players
            manager.SavePlayer("Luigi", 2, 20000);
            manager.SavePlayer("Sofia", 4, 52000);
            manager.SavePlayer("Gianni", 5, 51000);
            manager.SavePlayer("Stefano", 2, 21000);
            scoreList = manager.GetAllPlayers();
            //test if all players have been saved
            Assert.AreEqual(scoreList.Count(), 4);
            // test high score
            Assert.AreEqual(52000, manager.HighScore());
            //test reset ranking
            manager.Reset();
            scoreList = manager.GetAllPlayers();
            Assert.AreEqual(0, scoreList.Count);
            //test if the ranking is empty the maximum score is 0.
            Assert.AreEqual<int>(0, manager.HighScore());
        }

        [TestMethod]
        public void TestGameModelAndCollisions()
        {
            Collisions collisions = new Collisions();
            ISet<IPair<int, int>> walls = new HashSet<IPair<int, int>>();
            ISet<IPair<int, int>> pills = new HashSet<IPair<int, int>>();
            ISet<IPair<int, int>> ghostsHouse = new HashSet<IPair<int, int>>();
            for (int i = 0; i < 40; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    walls.Add(new Pair<int, int>(i, j));
                }
            }
            ghostsHouse.Add(new Pair<int, int>(0, 20));
            ghostsHouse.Add(new Pair<int, int>(1, 20));
            for (int i = 0; i < 40; i++)
            {
                for (int j = 21; j < 40; j++)
                {
                    pills.Add(new Pair<int, int>(i, j));
                }
            }

            IGameModel model = new GameModel();
            model.SetGameMap(new GameMap.Builder()
                    .MapSize(40, 40)
                    .PacManStartPosition(new Pair<int, int>(2, 20))
                    .Walls(walls)
                    .GhostsHouse(ghostsHouse)
                    .Pills(pills)
                    .PillScore(10)
                    .Build());
            model.InitializeNewGame();
            model.SetPacManDirection(Directions.DOWN);
            model.MoveEntitiesNextPosition();
            model.MoveEntitiesNextPosition();
            //Test pacman keeps the same direction
            Assert.AreEqual(Directions.DOWN, model.GetPacManDirection());
            //Test correct number of pacman lives
            Assert.AreEqual(3, model.GetPacManLives());
            IPair<int, int> pacmanPosition = model.GetPacManPosition();
            collisions.SetPacManPosition(pacmanPosition);
            var gst = new Dictionary<int, IPair<int, int>>();
            gst.Add(1, pacmanPosition);
            collisions.SetGhostsPositions(gst);
            //Test a simple collision case
            Assert.AreEqual(1, collisions.CheckPacManGhostsCollision().Count());
            //Test that the pills are removed
            Assert.AreEqual(false, model.GetPillsPositions().Contains(pacmanPosition));
            Assert.AreEqual(false, collisions.CheckPacManPillCollision(model.GetPillsPositions()));
            model.MoveEntitiesNextPosition();
            Assert.AreEqual(false, model.IsGameEnded());
            int oldLevel = model.GetLevelNumber();
            while (oldLevel == model.GetLevelNumber())
            {
                model.DecLevelTime();
            }
        }
    }
}
