using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pacman;
using System.Collections.Generic;

namespace Test
{
    [TestClass]
    public class PacmanTest
    {
        private const int XMAPSIZE = 28;
        private const int YMAPSIZE = 31;

        /**
         * Test for the correct creation of pacman entity.
         */
        [TestMethod]
        public void TestPacmanDirectionPosition()
        {
            PacmanImpl pacMan = new PacmanImpl.Builder()
                                    .CurrentDirection(Directions.LEFT)
                                    .Lives(3)
                                    .MapSize(XMAPSIZE, YMAPSIZE)
                                    .NoWalls(new HashSet<IPair<int, int>>())
                                    .StartPosition(new PairImpl<int, int>(0, 0))
                                    .Build();
            Assert.AreEqual(pacMan.CurrentDirection, Directions.LEFT);
            Assert.AreEqual(pacMan.GetPosition(), new PairImpl<int, int>(0, 0));
        }

        /**
         * Test for the correct movement of pacman.
         */
        [TestMethod]
        public void TestPacmanNextPosition()
        {
            ISet<IPair<int, int>> noWalls = new HashSet<IPair<int, int>>
            {
                new PairImpl<int, int>(0, 0),
                new PairImpl<int, int>(1, 0),
                new PairImpl<int, int>(XMAPSIZE - 1, 0)
            };
            PacmanImpl pacMan = new PacmanImpl.Builder()
                                        .CurrentDirection(Directions.RIGHT)
                                        .Lives(3)
                                        .MapSize(XMAPSIZE, YMAPSIZE)
                                        .NoWalls(noWalls)
                                        .StartPosition(new PairImpl<int, int>(0, 0))
                                        .Build();
            pacMan.NextPosition();
            Assert.AreEqual(pacMan.GetPosition(), new PairImpl<int, int>(1, 0));
            pacMan.NextPosition(); //this nextPosition do nothing because pacman can't go there (wall)
            Assert.AreEqual(pacMan.GetPosition(), new PairImpl<int, int>(1, 0));
            pacMan.CurrentDirection = Directions.LEFT;
            pacMan.NextPosition();
            Assert.AreEqual(pacMan.GetPosition(), new PairImpl<int, int>(0, 0));
            pacMan.NextPosition(); //this test is for toroidal conversion
            Assert.AreEqual(pacMan.GetPosition(), new PairImpl<int, int>(XMAPSIZE - 1, 0));
        }
    }
}
