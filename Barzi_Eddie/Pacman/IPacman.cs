﻿namespace Pacman
{
    /**
     * Interface that represent the pacman entity.
     */
    public interface IPacman : IEntity
    {
        /**
         * Set and Get Pacman direction
         */
        Directions CurrentDirection { get; set; }
        /**
         * <return>the remaining lives of PacMan</return>
         */
        int Lives { get; }
        /**
         * Decreases the life of PacMan and PacMan return to the startPosition.
         */
        void Kill();
    }
}
